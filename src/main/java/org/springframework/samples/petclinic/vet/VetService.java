package org.springframework.samples.petclinic.vet;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import org.springframework.samples.petclinic.model.BaseEntity;

@Entity
@Table(name = "vet_service")
public class VetService extends BaseEntity {
	@Column(name = "vet_id")
	private Integer vetId;
	@Column(name = "service_name")
	private String service_name;
	@Column(name = "service_price")
	private Integer service_price;

	public Integer getVetId() {
		return vetId;
	}

	public void setVetId(Integer vetId) {
		this.vetId = vetId;
	}

	public String getService_name() {
		return service_name;
	}

	public void setService_name(String service_name) {
		this.service_name = service_name;
	}

	public Integer getService_price() {
		return service_price;
	}

	public void setService_price(Integer service_price) {
		this.service_price = service_price;
	}


}
