package org.springframework.samples.petclinic.vet;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import org.springframework.samples.petclinic.model.BaseEntity;

@Entity
@Table(name = "vet_order")
public class VetOrders extends BaseEntity {

	@Column(name = "pet_id")
	private Integer pet_id;
	@Column(name = "vet_id")
	private Integer vetId;
	@Column(name = "vet_service_id")
	private Integer vet_service_id;
	@Column(name = "payment_method")
	private String payment_method;
	@Column(name = "amount")
	private Integer amount;
	@Column(name = "tax")
	private Integer tax;
	@Column(name = "total")
	private Integer total;
	@Column(name = "status")
	private String status;

	public Integer getPet_id() {
		return pet_id;
	}

	public void setPet_id(Integer pet_id) {
		this.pet_id = pet_id;
	}

	public Integer getVetId() {
		return vetId;
	}

	public void setVetId(Integer vetId) {
		this.vetId = vetId;
	}

	public Integer getVet_service_id() {
		return vet_service_id;
	}

	public void setVet_service_id(Integer vet_service_id) {
		this.vet_service_id = vet_service_id;
	}

	public String getPayment_method() {
		return payment_method;
	}

	public void setPayment_method(String payment_method) {
		this.payment_method = payment_method;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getTax() {
		return tax;
	}

	public void setTax(Integer tax) {
		this.tax = tax;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
