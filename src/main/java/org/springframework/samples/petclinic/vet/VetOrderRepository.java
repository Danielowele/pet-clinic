package org.springframework.samples.petclinic.vet;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VetOrderRepository extends JpaRepository<VetOrders, Integer> {

	List<VetOrders> findAllByVetId(Integer vetId);
}
