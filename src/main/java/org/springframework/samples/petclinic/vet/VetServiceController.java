package org.springframework.samples.petclinic.vet;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
@Controller
public class VetServiceController {
	private final VetServiceRepository vetServiceRepository;

	public VetServiceController(VetServiceRepository vetServiceRepository) {
		this.vetServiceRepository = vetServiceRepository;
	}

	@GetMapping("/gg")
	public List<VetService> showServiceListByVetId(@RequestParam int vet_id, Model model) {

		return vetServiceRepository.findAllByVetId(vet_id);
	}



}
