package org.springframework.samples.petclinic.owner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OwnerService {
	@Autowired
	private OwnerRepository ownerRepository;

	public boolean authenticateUser(String username, String password) {
		Owner owner = ownerRepository.findByUsername(username);

		// Check if the user exists and the password matches (you should use a secure password hashing method)
		return owner != null && owner.getPassword().equals(password);
	}
}
