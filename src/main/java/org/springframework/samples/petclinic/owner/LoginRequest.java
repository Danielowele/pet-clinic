package org.springframework.samples.petclinic.owner;

import jakarta.persistence.Column;

public class LoginRequest {
	private String username;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


	private String password;
	//add encryption later
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
