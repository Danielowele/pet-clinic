package org.springframework.samples.petclinic.Stripe;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.PaymentIntent;
import java.util.HashMap;
import java.util.Map;

public class StripePaymentProcessor {


	private static final String SECRET_KEY = "sk_test_your_secret_key";

	public static void main(String[] args) {
		// Set your secret key
		Stripe.apiKey = SECRET_KEY;

		// Create a PaymentIntent
		Map<String, Object> params = new HashMap<>();
		params.put("amount", 2000);
		params.put("currency", "usd");
		// Add more parameters as needed

		try {
			PaymentIntent paymentIntent = PaymentIntent.create(params);
			// Handle the paymentIntent, e.g., return its ID to the client
			System.out.println("PaymentIntent created: " + paymentIntent.getId());
		} catch (StripeException e) {
			// Handle Stripe API exceptions
			System.err.println("Error creating PaymentIntent: " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			// Handle other exceptions
			System.err.println("Unexpected error: " + e.getMessage());
			e.printStackTrace();
		}
	}
}
